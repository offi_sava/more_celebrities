@extends('layouts.app')
@section('content')
    @include('errors')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="container">
        <h3>Create post</h3>

        <div class="row">
            <br class="col-md-12 ">
                {!! Form::open(['route'=>['admin.store'],  'method'=>"post",'enctype'=>"multipart/form-data",]) !!}
                <br class="form-group">
                    <label for="party-time">Заголовок</label>
                   <input type="text" class="form-control" name="title" value="{{old('title')}}">
                <br> <input type="file" name="preview"></br>
                    <br>
                    <label for="party-time">Описание</label>
                    <textarea name="description" id="" cols="10" class="form-control"> {{old('description')}}</textarea>
                </br>
                    <label for="party-time">Теги</label>
                    <textarea name="hashtag" id="" cols="10" class="form-control"> {{old('description')}}</textarea>
                    </br>
                    <input name="names[]" type="file" multiple >
            <br>  <button class="btn btn-success"> Submit</button></br>
                </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>


@endsection