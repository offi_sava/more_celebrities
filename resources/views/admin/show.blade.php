@extends('layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="container">

        <h3>More Celebrities/id={{$article->id}}</h3>

        <div class="row">
            <div class="col-md-10 col=md-offset-1">
                <table class="table">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Title</td>
                        <td>Description</td>
                        <td>Preview</td>
                        <td>Hashtag</td>
                        <td>Date</td>
                        <td>Photos</td>

                    </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td>{{$article->id}}</td>
                            <td>{{$article->title}}</td>
                            <td>{{$article->description}}</td>
                            <td><img src="/uploads/{{$article->preview}}.jpg"
                                     width="150" height="130" ></td>
                            <td>
                                @foreach($tags as $tag)
                                {{$tag->name}}
                                @endforeach
                            </td>
                            <td>{{$article->created_at}}</td>
                            <td>
                                @foreach($images as $image)
                                    <img src="/uploads/{{$image->names}}.jpg"
                                         width="150" height="130" >
                                   @endforeach
                            </td>

                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection