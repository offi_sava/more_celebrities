@extends('layouts.app')
@section('content')
    @include('errors')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="container">
        <h3>Edit article # - {{$article->id}}</h3>

        <div class="row">
            <div class="col-md-12 ">
                {!! Form::open(['method'=>'put', 'route'=>['admin.update', $article->id]] ) !!}
                <div class="form-group">
                    <label for="party-time">Заголовок</label>
                    <input type="text" class="form-control" name="title" value="{{$article->title}}">
                      <input type="file" name="preview">   <br>
                    <label for="party-time">Описание</label>
                    <textarea name="description" id="" cols="10" class="form-control"> {{$article->description}}</textarea>
                    </br>
                    <label for="party-time">Теги</label>
                    <textarea name="hashtag" id="" cols="10" class="form-control"> </textarea>
                    </br>
                    <input name="names[]" type="file" multiple >
                    <button class="btn btn-warning"> Submit</button>
                </div>



                {!! Form::close() !!}
            </div>
        </div>
    </div>


@endsection