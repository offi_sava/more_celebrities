@extends('layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
   <div class="container">

           <h3>More.celebrities</h3>
           <a href="admin/create" class="btn btn-success">Create</a>
          <div class="row">
           <div class="col-md-10 col=md-offset-1">
               <table class="table">
                   <thead>
                   <tr>
                       <td>ID</td>
                       <td>Title</td>
                        <td>Preview</td>
                        <td>Date</td>
                   </tr>
                   </thead>
                   <tbody>
                   @foreach($articles as $article)
                   <tr>
                       <td>{{$article->id}}</td>
                       <td>{{$article->title}}</td>
                       <td><img src="/uploads/{{$article->preview}}.jpg"
                                width="150" height="130" ></td>
                       <td>{{$article->created_at}}</td>
                       <td>
                           <a href="{{route('admin.show', $article->id)}}">
                               <i class="glyphicon glyphicon-eye-open"></i>
                           </a>
                           <a href="{{route('admin.edit', $article->id)}}">
                               <i class="glyphicon glyphicon-edit"></i>
                           </a>
                           {!! Form::open(['method'=>'DELETE',
                           'route' => ['admin.destroy', $article->id]])!!}
                           <button onclick='return confirm("Are you sure?")'>
                               <i class="glyphicon glyphicon-remove"></i></button>
                           {!! Form::close() !!}

                       </td>
                   </tr>
                   @endforeach
                   </tbody>
               </table>
           </div>
   </div>
   </div>

@endsection