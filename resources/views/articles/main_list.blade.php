@extends('layouts.layout')
@section('title')
    <title>More Celebrities</title>
@endsection
@section('content')

    <!-- END #fh5co-header -->
    <div class="container-fluid">
        <div class="row fh5co-post-entry">
            @foreach($articles as $article)
                <article class="col-lg-3 col-md-3 col-sm-3 col-xs-6 col-xxs-12 animate-box">
                    <figure>
                        <a href="{{route('news.show', ['id'=>$article->slug])}}">
                            <img src="/uploads/{{$article->preview}}.jpg" alt="{{$article->id}}" class="img-responsive"></a>
                    </figure>
                    <span class="fh5co-meta"><a href="{{route('news.index')}}">News</a></span>
                    <h2 class="fh5co-article-title"><a
                                href="{{route('news.show', ['slug'=>$article->slug])}}">{{$article->title}}</a></h2>
                    <span class="fh5co-meta fh5co-date">{{$article->created_at}}</span>
                    <span class="fh5co-meta fh5co-date">      <img src="{{ URL::asset('http://lookshop.by/img/quick_view.svg') }}"
                                                                   width="20" height="20"
                                                                   alt="eye" class=""> {{$article->view_count}}</span>
                </article>
                <div class="clearfix visible-xs-block"></div>
            @endforeach
        </div>
        <center>
            {{$articles->links()}}
        </center>
    </div>
@endsection