@extends('layouts.layout')
@section('title')
    <title>Теги - More Celebrities</title>
@endsection
@section('content')
        <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-left content-article">
            <div class="row">
                <div class="col-md-12 animate-box">
                    <p align="center">Найдено по тегу:</p>
                    @foreach($articles as $article)
                        <center>
                            <figure class="animate-box">
                                <a href="{{route('news.show', ['id'=>$article->slug])}}">
                                    <img src="/uploads/{{$article->preview}}.jpg"
                                         width="300" height="255"
                                         alt="Image" class="img-responsive"></a>
                            </figure>
                            <strong>
                                <p>
                                    <a href="{{route('news.show', ['slug'=>$article->slug])}}">{{$article->title}}
                                    </a>
                                </p>
                                <p></p>
                            </strong>

                        </center>
                    @endforeach
                </div>
            </div>
        </div>


@endsection