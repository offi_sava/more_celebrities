@extends('layouts.layout')

@section('title')
    <title>{{$article->title}} - More Celebrities</title>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row fh5co-post-entry single-entry">
            <article
                    class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
                <figure class="animate-box">
                    <center><img src="/uploads/{{$article->preview}}.jpg" width="450" height="450" alt="Image"
                                 class="img-responsive">
                    </center>
                </figure>
                <span class="fh5co-meta animate-box"><a href="/news">News</a></span>

                <h1 itemprop="name headline">{{$article->title}}</h1>
                <span class="fh5co-meta fh5co-date animate-box">{{$article->created_at}}</span>
                 <span> <img src="{{ URL::asset('http://lookshop.by/img/quick_view.svg') }}"
                             width="20" height="20"
                             alt="eye" class=""> {{$article->view_count}}</span>

                </span>
                <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-left content-article">
                    <div class="row">
                        <div class="col-md-12 animate-box">
                            <p>{!! $article->description !!}</p>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($images as $image)
                            <div class="col-md-4 animate-box">

                                <figure>
                                    <a href="/uploads/{{$image->names}}.jpg"> <img src="/uploads/{{$image->names}}.jpg"
                                                                                   alt="" class="img-responsive">
                                    </a></figure>

                            </div>      @endforeach
                    </div>
                </div>

                <p>А что будет дальше?</p>
                @if(count($tags))
                    <div class="hashtagg">Теги:
                        @foreach($tags as $tag)
                            <a href="{{route('news.tag', ['tag'=>$tag->slug])}}" class="href-bord">
                                {{$tag->name}}</a>
                        @endforeach
                    </div>
                @endif

            </article>
        </div>
    </div>

@endsection


