
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @yield('title')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" Your celebrity magazine." />
    <meta name="keywords" content="" />
    <meta name="author" content="FREEHTML5.CO" />



    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="{{ URL::asset('star_1.ico') }}">
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic|Roboto:400,300,700' rel='stylesheet' type='text/css'>
    <!-- Animate -->
    <link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}">
    <!-- Icomoon -->
    <link rel="stylesheet" href="{{ URL::asset('css/icomoon.css') }}">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">


    <!-- Modernizr JS -->
    <script src="{{ URL::asset('js/modernizr-2.6.2.min.js') }}"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>

    <![endif]-->
    <!-- jQuery -->
    <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <!-- jQuery Easing -->
    <script src="{{ URL::asset('js/jquery.easing.1.3.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <!-- Waypoints -->
    <script src="{{ URL::asset('js/jquery.waypoints.min.js') }}"></script>
    <!-- Main JS -->
    <script src="{{ URL::asset('js/main.js') }}"></script>
    <style type="text/css">

        .hashtagg {
            border: 1px solid grey;
            text-align:left;
            padding: 10px;
        }
        .href-bord{
            border: 1px solid grey;
            padding: 5px;
            color: grey;
        }

    </style>
</head>
<body>
<div id="fh5co-offcanvas">
    <a href="#" class="fh5co-close-offcanvas js-fh5co-close-offcanvas"><span><i class="icon-cross3"></i> <span>Close</span></span></a>
    <div class="fh5co-bio">
        <figure>
            <img src="{{ URL::asset('star.jpg') }}" alt="More Celebrities" class="img-responsive">
        </figure>
        <h3 class="heading">О нас</h3>
        <h2>More Celebrities</h2>
        <p>Всё самое новое и интересное мы собираем здесь для тебя! </p>
        <ul class="fh5co-social">

            <li><a href="https://www.instagram.com/more.celebrities/"><i class="icon-instagram"></i></a></li>
        </ul>
    </div>

    <div class="fh5co-menu">
        <div class="fh5co-box">
            <h3 class="heading">Категории</h3>
            <ul>
                <li><a href="{{route('news.index')}}">Новости</a></li>
                <li><a href="#">..в разработке..</a></li>
            </ul>
        </div>
        <div class="fh5co-box">

            <h3 class="heading">Поиск</h3>
            {!! Form::open(['route'=>['news.search'],  'method'=>"get"]) !!}

            <div class="form-group">
                <p>     <input type="text" class="form-control" name="q" placeholder=" "></p>
                <button class="btn btn-success"> Найти статью</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- END #fh5co-offcanvas -->
<header id="fh5co-header">

    <div class="container-fluid">

        <div class="row">
            <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
            <ul class="fh5co-social">

                <li></li>
            </ul>
            <div class="col-lg-12 col-md-12 text-center">
                <h1 id="fh5co-logo"><a href="{{route('news.index')}}">
                        <img src="{{ URL::asset('star2.jpg') }}" width="180" height="180" alt="More Celebrities" >
                    </a></h1>
            </div>

        </div>

    </div>

</header>

@yield('content')

<footer id="fh5co-footer">
    <p><small>&copy; 2018. Your celebrity magazine. All Rights Reserverd. <br> With love from Riabova A.</a>  </small></p>


</footer>
</body>
</html>

