<?php

namespace App;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use \Conner\Tagging\Taggable;
    use Sluggable;
    protected $fillable = ['title', 'description', 'created_at'];

    public function images(){
        return $this->hasMany('App\Image','article_id');
    }
    public function tags(){
        return $this->belongsToMany('App\Tag','articles_tags');
    }
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(){
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
