<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Mail;


class MailController extends Controller
{
    public function send(Request $request){
        $this->validate($request, [
            'email' => 'required',
        ]);
        Mail::raw($request['email'],function ($message){
            $message->to('mo.celebrities@gmail.com','To More Celebreties')->subject('Follow');
            $message->from('mo.celebrities@gmail.com','More Celebreties');

        } );
        return back()->with('success',' Upload successfully');
    }
}
