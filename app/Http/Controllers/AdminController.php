<?php

namespace App\Http\Controllers;
use App\ArticlesTag;
use App\Tag;
use Illuminate\Http\Request;
use App\Article;
use App\Image;
use Conner\Tagging\Taggable;
use Illuminate\Foundation\Validation\ValidatesRequests;

class AdminController extends Controller
{
    use ValidatesRequests;
    use Taggable;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $myArticle = Article::all()->sortByDesc('created_at');
        return view('admin.index', ['articles' => $myArticle]);
    }
    public function create(){
        return view('admin.create');
    }
    public function store(Request $request){
        $myArticle = new Article();
        $myArticle->fill($request->all());
        $row = $request->file('preview');
        $name= md5($row->getClientOriginalName() . uniqid());
        $destinationPath = public_path('uploads');
        $row->move($destinationPath, $name .'.jpg');
        $myArticle['preview'] = $name;
        $myArticle->save();
        $maxIdArticle = Article::all()->max('id');

        $row = $request->file('names');
        for($i=0;$i<count($request->file('names'));$i++){
            $photo = new Image();
            $names[$i] = md5($row[$i]->getClientOriginalName() . uniqid());
            $destinationPath = public_path('uploads');
            $row[$i]->move($destinationPath, $names[$i] . '.jpg');
            $photo['names'] = $names[$i];
            $photo['article_id'] = $maxIdArticle;
            $photo->save();
        }

        $arr_tag = explode(',',$request['hashtag']);
        for($i=0;$i<count($arr_tag);$i++){
            $tag = new Tag();
            $relationTable = new ArticlesTag();
            $tag['name'] = $arr_tag[$i];
            $tag->save();
            $maxIdTag = Tag::all()->max('id');
            $relationTable['article_id'] = $maxIdArticle;
            $relationTable['tag_id'] = $maxIdTag;
            $relationTable->save();

        }
        return redirect()->route('admin.index');
    }
    public function edit($id){
        $myArticle = Article::find($id);
        return view('admin.edit', ['article'=>$myArticle]);
    }
    public function update(Request $request, $id){
        $myArticle = Article::find($id);
        $myArticle->fill($request->all());
        $myArticle->save();
        return redirect()->route('admin.index');
    }
    public function show($id){
        $article = Article::find($id);
        $photo = Article::find($id)->images;
        $tags = Article::find($id)->tags()->get();
        return view('admin.show', ['article'=>$article, 'images'=>$photo,'tags'=>$tags]);
    }
    public function destroy($id){
        ArticlesTag::where('article_id',$id)->delete();
        $article = Article::find($id);
        Article::find($id)->tags()->delete();
        Image::where('article_id',$id)->delete();
        $article->delete();
        return redirect()->route('admin.index');
    }



}
