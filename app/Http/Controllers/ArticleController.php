<?php

namespace App\Http\Controllers;
use App\Article;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
class ArticleController extends Controller
{
    use ValidatesRequests;
    public function index(Request $request){
        $myArticle = Article::orderBy('created_at', 'DESC')->paginate(8);
       // if ($request->ajax()) {
         //   return view('articles.main_list', ['articles'=>$myArticle]);
        //}
      return view('articles.main', ['articles'=>$myArticle]);
    }
    public function show($slug){
        $myArticle = Article::where('slug', $slug)->firstOrFail();
        $photo = $myArticle->images;
        $tags = $myArticle->tags()->get();
        $myArticle->increment('view_count');
        return view('articles.note', ['article'=>$myArticle, 'images'=>$photo,'tags'=>$tags]);
    }
    public function search(Request $request){
        $this->validate($request, [
            'q' => 'required',
        ]);
        $text = $request['q'];
        $myArticle = Article::where('description', 'LIKE','%'.$text.'%')->get();
       return view('articles.search', ['articles'=>$myArticle, 'request'=>$text]);

    }
    public function tag($slug){
        $tag = Tag::where('slug', 'LIKE','%'.$slug.'%')->get();
        $articles = Tag::find($tag[0]->id)->articles()->get();
         return view('articles.tag', ['articles'=>$articles]);

    }





}
