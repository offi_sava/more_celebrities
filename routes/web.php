<?php


Auth::routes();

Route::get('home', 'HomeController@index')->name('home');

Route::resource('gov/admin', 'AdminController');

Route::get('', 'ArticleController@index')->name('news.index');

//Route::get('news', 'ArticleController@index')->name('news.index');
Route::get('news/{slug}', 'ArticleController@show')->name('news.show');
Route::get('/search', 'ArticleController@search')->name('news.search');
Route::get('/tag/{slug}', 'ArticleController@search')->name('news.tag');


Route::get('tag/{slug}', 'ArticleController@tag')->name('news.tag');
Route::post('mail/', 'MailController@send')->name('mail.send');


